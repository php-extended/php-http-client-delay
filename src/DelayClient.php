<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-delay library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Stringable;

/**
 * DelayClient class file.
 * 
 * This client is an implementation of a client that insert delays between
 * each request.
 * 
 * @author Anastaszor
 */
class DelayClient implements ClientInterface, Stringable
{
	
	/**
	 * The inner client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * The configuration for the delay client.
	 * 
	 * @var DelayConfiguration
	 */
	protected DelayConfiguration $_config;
	
	/**
	 * Builds a new DelayClient with the given inner client and configuration.
	 * 
	 * @param ClientInterface $client
	 * @param DelayConfiguration $config
	 */
	public function __construct(ClientInterface $client, DelayConfiguration $config)
	{
		$this->_client = $client;
		$this->_config = $config;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		$delayMs = $this->_config->getDelayMs();
		
		if($request->hasHeader('X-Php-Delay-Ms'))
		{
			$delayMs = (int) $request->getHeaderLine('X-Php-Delay-Ms');
			$request = $request->withoutHeader('X-Php-Delay-Ms');
		}
		
		if(0 < $delayMs)
		{
			\usleep($delayMs * 1000);
		}
		
		return $this->_client->sendRequest($request);
	}
	
}
