<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-delay library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Stringable;

/**
 * DelayConfiguration class file.
 * 
 * This class represents the configuration for the DelayClient.
 * 
 * @author Anastaszor
 */
class DelayConfiguration implements Stringable
{
	
	/**
	 * The quantity of milliseconds of delay.
	 * 
	 * @var integer
	 */
	protected int $_delayMs = 0;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the number of milliseconds of delay.
	 * 
	 * @param integer $delayMs
	 * @return DelayConfiguration
	 */
	public function setDelayMs(int $delayMs) : DelayConfiguration
	{
		$this->_delayMs = 0 < $delayMs ? $delayMs : 0;
		
		return $this;
	}
	
	/**
	 * Gets the number of milliseconds of delay.
	 * 
	 * @return integer
	 */
	public function getDelayMs() : int
	{
		return $this->_delayMs;
	}
	
}
