<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-delay library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\DelayClient;
use PhpExtended\HttpClient\DelayConfiguration;
use PhpExtended\HttpMessage\Request;
use PhpExtended\HttpMessage\Response;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * DelayClientTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\DelayClient
 *
 * @internal
 *
 * @small
 */
class DelayClientTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var DelayClient
	 */
	protected DelayClient $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$beginTime = \microtime(true);
		
		$this->_object->sendRequest(new Request());
		
		$endTime = \microtime(true);
		
		$this->assertGreaterThan(1, $endTime - $beginTime);
	}
	
	public function testItWorksCustomHeader() : void
	{
		$beginTime = \microtime(true);
		
		$request = new Request();
		$request = $request->withHeader('X-Php-Delay-Ms', 2000);
		$this->_object->sendRequest($request);
		
		$endTime = \microtime(true);
		
		$this->assertGreaterThan(2, $endTime - $beginTime);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface
		{
			
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				return new Response();
			}
			
		};
		
		$config = new DelayConfiguration();
		$config->setDelayMs(1000);
		
		$this->_object = new DelayClient($client, $config);
	}
	
}
