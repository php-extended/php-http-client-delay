<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-delay library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\DelayConfiguration;
use PHPUnit\Framework\TestCase;

/**
 * DelayConfigurationTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\DelayConfiguration
 *
 * @internal
 *
 * @small
 */
class DelayConfigurationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var DelayConfiguration
	 */
	protected DelayConfiguration $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testDelayMs() : void
	{
		$this->assertEquals(0, $this->_object->getDelayMs());
		$this->_object->setDelayMs(1000);
		$this->assertEquals(1000, $this->_object->getDelayMs());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new DelayConfiguration();
	}
	
}
