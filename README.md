# php-extended/php-http-client-delay

An http client that adds delays between requests for getting around time based bot detection.

![coverage](https://gitlab.com/php-extended/php-http-client-delay/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-http-client-delay/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-http-client-delay ^8`


## Basic Usage

This library is to make a man in the middle for http request and response and 
deny all the requests that does to unacceptable or unsafe domains.

```php

/* @var $client  \Psr\Http\Client\ClientInterface   */ // psr-18
/* @var $request \Psr\Http\Message\RequestInterface */ // psr-7

$client = new DelayClient($client);
$response = $client->sendRequest($request);

/* @var $response \Psr\Http\Message\ResponseInterface */

```

This library supports the custom header `X-Php-Delay-Ms` that represents
the number of milliseconds of delays that must be applied for a specific request.


## License

MIT (See [license file](LICENSE)).
